# A detailed characterization of complex networks using Information Theory
<span style="color:red">
This Article contains errors.

After publication of the Article it was brought to Authors' attention that since the quantifier they were proposing for networks consider the node indices to build the random-walk-based distribution, if these indices are permuted without altering the network structure, the results for Network Fisher may change.
This is a limitation of the original study, which should be acknowledged.

For more information, read the [correction letter](Corrections_on_Scientific_Reports.pdf) submitted to the editorial board in the data repository.

These limitations do not affect the overall conclusions of the Article.
</span>

## This repository contains all the data and code used to develop our research in the related paper submitted to Scientific Reports. 

#### The repository is organized as follows:
- `/datasets`  - the real networks evaluated during our analysis; 
- `/models` - the simulated data generated using the scripts for synthetic networks, i.e., *erdos_renyi.R*, *barabasi.R*, *fitness.R*, *aging.R*, *deg_seq.R*, *watts_strogatz.R*; 
- `/networks` - the results from our real networks analysis; 
- `/topologies`- illustration of the synthetic networks studied, alongside the *topologies.psd* file corresponding to our *overview* figure. 

#### Also, we have the following scripts:
- `barabasi.R`- it executes the experiment for Barabási-Albert networks with non-linear preferential attachment and it generates the figures presented in `/models/Scale Free/`.
- `fitness.R`- it executes the experiment for Bianconi-Barabási networks with uniform fitness distribution and it generates the figures presented in `/models/Scale Free/` .
- `aging.R`- it executes the experiment for the Aging model and it generates the figures presented in `/models/Scale Free/` .
- `deg_seq.R`- it executes the experiment for the configuration model and it generates the figures presented in `/models/Scale Free/` .
- `erdos-renyi.R`- it executes the experiment for Erdos-Renyi networks and it generates the figures presented in `/models/Erdos Renyi/` .
- `watts-strogatz.R`- it executes the experiment for Watts-Strogatz networks and it generates the figures presented in `/models/Small World/` .
- `watts-strogatz.R`- it executes the experiment for *k-ring* networks and it generates the figures presented in `/models/Small World/`  as `k-ring_*.eps`; the file name varies according to "Shannon Entropy", "Fisher Information Measure", and "Link Density".
- `helpers.R`- contains our main script for evaluating the networks using the Shannon-Fisher plane. 
- `measures.R`- contains the *Network Entropy* and *Fisher Information Measure* code, i.e., `FisherNetwork`, and `EvaluateNetwork` for evaluating the real networks. 
- `networks.R` - contains the code for evaluating the real networks in `/datasets/*`  and stores the results in `/networks/ *`. 
- `smallworldness.R`- it calculates the smallworldness for real networks in `networks/real_networks_new.csv` and stores the results in `results.csv`. 
- `topologies.R`- it creates the graphs presented into `/topologies/*`; these graphs were later used to create the illustration saved as *topologies.psd*. 
- `plot3d.py`- it creates the 3D plots for Erdos-Renyi and Watts-Strogatz networks, i.e., *ER-3D.pdf* and *WS-3D.pdf*. 

## Software requirements

This code version is tested on Linux and MacOS Mojave operating systems:

- Ubuntu 16.04
- MacOS Mojave 10.14.5

To improve our performance, we take advantages of multi-core parallel processing libraries, for this reason, 
there is a incompatibility for Windows OS.

**Installing R 3.4.4 on Ubuntu 16.04**

```sh
$ sudo echo "deb http://cran.rstudio.com/bin/linux/ubuntu xenial/" | sudo tee -a /etc/apt/sources.list
$ gpg --keyserver keyserver.ubuntu.com --recv-key E084DAB9
$ gpg -a --export E084DAB9 | sudo apt-key add -
$ sudo apt-get update
$ sudo apt-get install r-base r-base-dev
```

The installation process takes around 40 seconds. 

## Installation Guide

Prior to running the script experiments, we need to install the following required packages for R: 

``
install.packages(c('igraph', 'foreach', 'doParallel', 'Matrix', 'pryr', 'ggplot2'))
``

The latest version of these packages were used by December 2018: 

```
igraph             1.2.2    
foreach            1.4.4    
doParallel         1.0.11   
Matrix             1.2-12   
plyr               1.8.4    
ggplot2            3.1.0    
```

The installation process takes around 60 seconds. 

## Running the scripts

Later, you can run the scripts using `Rscript` to call the experiment execution, this will automatically generate the data results and their plots. 

*This repository already has the actual results described in the article, if you are going to generate new ones, we recommend you to delete the original, as you can rollback further, if you need to*

```sh
$ Rscript erdos_renyi.R # Random networks
$ Rscript watts_strogatz.R # Small world networks
$ Rscript barabasi.R # Scale free networks
$ Rscript networks.R # Real world networks
```

This experiment is very computational expensive, and it takes a lot of time to execute as we evaluate more than 50000 networks. 

## Demo

As the full simulation can take a few days to execute it, we recommend you to first execute a demo version that follows a procedure very similar to the real experiment: 

```sh
$ Rscript demo.R
```

The running process using a Macbook Pro Retina 13' Early 2015 with the Intel Core i5 2.7 GHz, it took around 5 minutes to finish it, and the expected results are: 

<img src="https://gitlab.com/cristophersfr/fisher-networks/raw/master/models/demo/demo_hxf.png"  width="200" height="200">
<img src="https://gitlab.com/cristophersfr/fisher-networks/raw/master/models/demo/demo_pxh.png"  width="200" height="200">
<img src="https://gitlab.com/cristophersfr/fisher-networks/raw/master/models/demo/demo_pxf.png"  width="200" height="200">

These results as high dimension images are found into `models/demo/*`. The same apply for all the others experiment. 

## Extra

In Section 4, for real networks results discussion, we present the smallworldness value for these networks, you can also calculate these values using the following script:

```sh
$ Rscript smallworldness.R
```

Finally, if you have any questions or you want to report anaything, feel free to reach me at: cristopher@laccan.ufal.br. 
