# Metrics to be computed

# Shannon entropy 'H'
# probs: vector of probabilities from the Bandt-Pompe distribution
shannon_entropy <- function(probs, normalized=FALSE)
{
  # consider only the non-zero values
  p = which(probs > 1e-30)
  
  # considering log base n
  entropy = -sum(probs[p]*log(probs[p]))
  
  # if normalized, the log base is irrelevant
  if(normalized)
  {
    entropy = entropy/log(length(probs))
  }
  
  return(entropy)
}

# Fisher Information
# probs: vector of probabilities from the Bandt-Pompe distribution
fisher_information <- function(probs)
{
  # the number of probabilities
  N = length(probs)
  
  # the normalization constant
  F0 = 1/2
  
  #print((sqrt(probs[2:N]) - sqrt(probs[1:(N-1)]))^2)
  
  # the fisher information
  f = F0*sum((sqrt(probs[2:N]) - sqrt(probs[1:(N-1)]))^2)
  
  return(f)
}

jensen_shannon <- function(probs_1, probs_2){
  divergence <- c(rep(0,nrow(probs_1)))
  for(i in 1:nrow(probs_1)){
    divergence[i] <- (1/log(2))*(shannon_entropy(0.5*(probs_1[i,] + probs_2[i,])) - 0.5*(shannon_entropy(probs_1[i,]) + shannon_entropy(probs_2[i,])))
  }
  return(divergence)
}

random_walk_betwenness <- function(adjacencyMatrix){
  n <- ncol(adjacencyMatrix)
  probMatrix <- sparseMatrix(i = 1:n, j = 1:n, x = 0)
  for(i in 1:n) {
    neighbors <- which(adjacencyMatrix[i,] > 0)
    k <- length(neighbors)
    if(k > 0){
      probMatrix[i,neighbors] <- 1/k
    }
  }
  return(probMatrix)
}

# Linking probability of the Erdos Renyi network
prob_erdos <- function(adjacencyMatrix){
  degrees <- c()
  for(i in 1:nrow(adjacencyMatrix)){
    degrees[i] <- length(which(adjacencyMatrix[i,] > 0))
  }
  pErdos <- sum(degrees)/(length(degrees)*(length(degrees)-1))
  return(pErdos)
}