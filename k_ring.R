library(igraph)
library(parallel)
library(foreach)
library(doParallel)
library(Matrix)
library(pryr)

source("measures.R")
source("helpers.R")

nCluster <- detectCores() - 2
cl <- makeCluster(nCluster, type = "FORK")
registerDoParallel(cl)

output <- "models/Small World/k_ring_results"

if(!file.exists(paste(output,".txt", sep=""))){
  file.create(paste(output,".txt", sep=""))
}

k <- 1:500
N <- 1000
configs <- expand.grid(N, k)
names(configs) <- c("N", "k")
n <- nrow(configs)

sapply(1:n, function(i){
  print(configs[i,])
  graph <- sample_smallworld(1, configs[i,]$N, configs[i,]$k, 0)
  xi <- edge_density(graph, loops=FALSE)
  adjacencyMatrix <- as_adj(graph, sparse = TRUE)
  result <- parEvaluateNetwork(adjacencyMatrix, "k-ring")
  cat(result, xi, file = paste(output,".txt", sep=""), append = TRUE, fill = TRUE, sep = ",")
})

stopCluster(cl)

library(ggplot2)
results  <- read.csv("models/Small World/k_ring_results.txt", header = FALSE)
colnames(results) <- c("Network", "N", "H", "F", "xi", "p")

g <- ggplot(results, aes(p, F))
g <- g + geom_point(size = 3)
g <- g + scale_x_continuous(limits = c(0, 1)) + scale_y_continuous(limits = c(0, 1))
g <- g + labs(x = "Link Density", y = "Fisher Information Measure")
g <- g + theme(legend.title = element_blank(),legend.position = "bottom", legend.text = element_text(size = 12), legend.direction = "horizontal")
g <- g + theme(axis.text=element_text(size=14), axis.title=element_text(size=14))
g <- g + theme(panel.background = element_rect(fill = "white", colour = "grey50"), axis.line = element_line(size = 1, colour = "grey80"))
ggsave(filename = "k_ring_pxf.eps", device = "eps", path = "models/Small World/", dpi = 300, width = 14, height = 15, units = "cm")

g <- ggplot(results, aes(p, H))
g <- g + geom_point(size = 3)
g <- g + scale_x_continuous(limits = c(0, 1)) + scale_y_continuous(limits = c(0, 1))
g <- g + labs(x = "Link Density", y = "Network Entropy")
g <- g + theme(legend.title = element_blank(),legend.position = "bottom", legend.text = element_text(size = 12), legend.direction = "horizontal")
g <- g + theme(axis.text=element_text(size=14), axis.title=element_text(size=14))
g <- g + theme(panel.background = element_rect(fill = "white", colour = "grey50"), axis.line = element_line(size = 1, colour = "grey80"))
ggsave(filename = "k_ring_pxh.eps", device = "eps", path = "models/Small World/", dpi = 300, width = 14, height = 15, units = "cm")