library(igraph)
library(parallel)
library(foreach)
library(doParallel)
library(Matrix)
library(pryr)

source("measures.R")
source("helpers.R")

nCluster <- detectCores() - 2
cl <- makeCluster(nCluster, type = "FORK")
registerDoParallel(cl)

##### Small World/Watts-Strogatz Networks #####

output <- "models/Small World/results_small_world_networks"

if(!file.exists(paste(output,".txt", sep=""))){
  file.create(paste(output,".txt", sep=""))
}

# Rewiring probability

configs <- c()

p <- seq(0.0, 0.01, 0.001)
p <- append(p, seq(0.01, 1, 0.01))
k <- 1:500
configs <- expand.grid(1000, p, k)

names(configs) <- c("N", "p", "k")
nit <- nrow(configs)

sapply(1:nit, function(i){
  print(configs[i,])
  graph <- sample_smallworld(1, configs[i,]$N, configs[i,]$k, configs[i,]$p)
  density <- edge_density(graph)
  adjacencyMatrix <- as_adj(graph, sparse = TRUE)
  result <- FisherNetwork(adjacencyMatrix, "Watts-Strogatz")
  cat(result, configs[i,]$k, configs[i,]$p, density,
      file = paste(output,".txt", sep=""), append = TRUE, fill = TRUE)
})

stopCluster(cl)

##### Plot results #####
library(ggplot2)
results <- read.table("models/Small World/results_small_world_networks.txt")
colnames(results) <- c("Network","N", "H", "F", "k", "beta", "p")
kring <- subset(results, results$beta == 0)
random <- subset(results, results$beta == 1)
smallworld <- subset(results, results$beta != 0 & results$beta != 1)

g <- ggplot()
g <- g + geom_point(data = smallworld, mapping = aes(H,  F, colour = beta), shape = 1, size = 3, inherit.aes = FALSE)
g <- g + geom_point(data = kring, mapping = aes(H,  F), colour = "red", shape = 6, size = 3, inherit.aes = FALSE)
g <- g + geom_point(data = random, mapping = aes(H,  F), colour = "blue", shape = 2, size = 3, inherit.aes = FALSE)

g <- g + scale_x_continuous(limits = c(0, 1)) + scale_y_continuous(limits = c(0, 1))
g <- g + labs(x = "Network Entropy", y = "Fisher Information Measure", colour = expression(beta))
g <- g + theme(legend.position="right", legend.text = element_text(size = 14))
g <- g + theme(axis.text=element_text(size=18), axis.title=element_text(size=18), legend.title = element_text(size = 22))
g <- g + theme(panel.background = element_rect(fill = "white", colour = "grey50"), axis.line = element_line(size = 1, colour = "grey80"))
ggsave(filename = "watts_strogatz_hxf.eps", device = "eps", path = "models/Small World/", dpi = 300, width = 16.5, height = 14, units = "cm")


g <- ggplot()
g <- g + geom_point(data = smallworld, mapping = aes(p,  F, colour = beta), shape = 1, size = 3, inherit.aes = FALSE)
g <- g + geom_point(data = kring, mapping = aes(p,  F), colour = "red", shape = 6, size = 3, inherit.aes = FALSE)
g <- g + geom_point(data = random, mapping = aes(p,  F), colour = "blue", shape = 2, size = 3, inherit.aes = FALSE)

g <- g + scale_x_continuous(limits = c(0, 1)) + scale_y_continuous(limits = c(0, 1))
g <- g + labs(x = "Link Density", y = "Fisher Information Measure", colour = expression(beta))
g <- g + theme(legend.position="right", legend.text = element_text(size = 14))
g <- g + theme(axis.text=element_text(size=18), axis.title=element_text(size=18), legend.title = element_text(size = 22))
g <- g + theme(panel.background = element_rect(fill = "white", colour = "grey50"), axis.line = element_line(size = 1, colour = "grey80"))
ggsave(filename = "watts_strogatz_pxf.eps", device = "eps", path = "models/Small World/", dpi = 300, width = 16.5, height = 14, units = "cm")

g <- ggplot()
g <- g + geom_point(data = smallworld, mapping = aes(p,  H, colour = beta), shape = 1, size = 3, inherit.aes = FALSE)
g <- g + geom_point(data = kring, mapping = aes(p,  H), colour = "red", shape = 6, size = 3, inherit.aes = FALSE)
g <- g + geom_point(data = random, mapping = aes(p,  H), colour = "blue", shape = 2, size = 3, inherit.aes = FALSE)

g <- g + scale_x_continuous(limits = c(0, 1)) + scale_y_continuous(limits = c(0, 1))
g <- g + labs(x = "Link Density", y = "Network Entropy", colour = expression(beta))
g <- g + theme(legend.position="right", legend.text = element_text(size = 14))
g <- g + theme(axis.text=element_text(size=18), axis.title=element_text(size=18), legend.title = element_text(size = 22))
g <- g + theme(panel.background = element_rect(fill = "white", colour = "grey50"), axis.line = element_line(size = 1, colour = "grey80"))
ggsave(filename = "watts_strogatz_pxh.eps", device = "eps", path = "models/Small World/", dpi = 300, width = 16.5, height = 14, units = "cm")
