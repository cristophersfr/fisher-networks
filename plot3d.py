#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 17 13:40:23 2019

@author: cristopher
"""
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import pandas as pd

plt.style.use('seaborn-white')


'''
Plotting 3D results for ER
'''

ER = pd.read_csv("models/Erdos Renyi/erdos_renyi_monte_carlo_N_4.txt", 
                 names = (["Network", "N", "H", "F", "xi", "p"]))

ER = ER[ER['N'] == 1000]

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.scatter(ER['p'], ER['H'], ER['F'], c="#549F93")
           
ax.view_init(30, 45)
           
ax.set_xlim(left=0, right=1)
ax.set_ylim(bottom=0, top=1)
ax.set_zlim(bottom=0, top=1)

ax.set_xlabel('Link Density')
ax.set_ylabel('Network Entropy')
ax.set_zlabel('Fisher Information Measure')
ax.set_aspect('equal')
ax.figure.set_size_inches(5,5)

with PdfPages('ER-3D.pdf') as pdf:
    pdf.savefig(fig,facecolor='w', edgecolor='w')

'''
Plotting 3D results for ER
'''

WS = pd.read_csv("models/Small World/results_small_world_networks.csv",
                 names = (["Network","N", "H", "F", "k", "beta", "p"]))

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

N = 256
vals = np.ones((N, 4))
vals[:, 0] = np.linspace(20/256, 89/256, N)
vals[:, 1] = np.linspace(42/256, 176/256, N)
vals[:, 2] = np.linspace(67/256, 247/256, N)
cmap = mpl.colors.ListedColormap(vals)
                                               
norm = mpl.colors.Normalize(vmin=0, vmax=1)

p = ax.scatter(WS['p'], WS['H'], WS['F'], c=WS['beta'], cmap = cmap, norm = norm)

fig.colorbar(p, fraction=0.02, pad=0.04)

ax.view_init(30, 45)

ax.set_xlim(left=0, right=1)
ax.set_ylim(bottom=0, top=1)
ax.set_zlim(bottom=0, top=1)
           
ax.set_xlabel('Link Density')
ax.set_ylabel('Network Entropy')
ax.set_zlabel('Fisher Information Measure')
ax.set_aspect('equal')

fig.set_size_inches(5,5)

with PdfPages('WS-3D.pdf') as pdf:
    pdf.savefig(fig,facecolor='w', edgecolor='w')
